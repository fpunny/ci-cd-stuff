const express = require('express');
const app = express();
const port = 3000;

app.get('/', (req, res) => res.send('Goodbye World!'));

// TODO: Add process.env.PORT
app.listen(port, () => console.log(`Example app listening on port ${ process.env.PORT || port}!`));